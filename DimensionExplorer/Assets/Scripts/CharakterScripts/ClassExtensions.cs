using UnityEngine;
using System.Collections.Generic;

namespace AttackLogic
{
    public static class Attack
    {
        public static List<GameObject> colorableObjects = new List<GameObject>();

        #region Attack
        /// <summary>
        /// performs an Attack depending on the values that be given by the Object who attacks
        /// </summary>
        /// <param name="attacker"></param>
        public static void DoAttack(this CanDealDamage attacker)
        {
            #region scale and lcalize collider
            Vector3 colliderSize = new Vector3(attacker.attackSize.x, 2, attacker.attackSize.y);
            Vector3 colliderCenter = Vector3.zero;
            if (attacker.direction == Vector3.left || attacker.direction == Vector3.right || attacker.direction == Vector3.back || attacker.direction == Vector3.forward)
            {
                colliderCenter = attacker.worldPosition + (attacker.direction * (attacker.attackRange + colliderSize.z / 2));
            }
            else
            {
                colliderCenter = attacker.worldPosition + (attacker.direction * (attacker.attackRange + colliderSize.z / 2.85f));
            }
            Collider[] hittedColliders = Physics.OverlapBox(colliderCenter, colliderSize / 2, attacker.rotation);
            #endregion

            #region debugging
            //Zeigt die hitbox an um zu sehen ob die Position und die Skallierung stimmt
            /*
            GameObject gameObject = GameObject.CreatePrimitive(PrimitiveType.Cube);
            if (attacker.direction == Vector3.left || attacker.direction == Vector3.right || attacker.direction == Vector3.back || attacker.direction == Vector3.forward)
            {
                gameObject.transform.position = attacker.worldPosition + (attacker.direction * (attacker.attackRange + colliderSize.z / 2));
            }
            else
            {
                gameObject.transform.position = attacker.worldPosition + (attacker.direction * (attacker.attackRange + colliderSize.z / 2.85f));
            }
            gameObject.transform.localScale = colliderSize;
            gameObject.transform.Rotate(0, attacker.rotation.eulerAngles.y, 0);
            */
            #endregion


            for (int i = 0; i < hittedColliders.Length; i++)
            {
                if (hittedColliders[i].tag == "Enemy" && attacker == PlayerLogic.player)
                {
                    for (int j = 0; j < EnemyLogic.enemies.Count; j++)
                    {
                        if (EnemyLogic.enemies[j].sceneObject == hittedColliders[i].gameObject)
                        {
                            colorableObjects.Add(EnemyLogic.enemies[j].sceneObject);
                            EnemyLogic.enemies[j].GetDamage(attacker.attackValue);
                        }
                    }
                }
                else if (hittedColliders[i].tag == "Player" && attacker != PlayerLogic.player)
                {
                    PlayerLogic.player.GetDamage(attacker.attackValue);
                }
            }
        }

        public static void DoAttack(this CanDealDamage attacker, out bool someoneWasHit)
        {
            #region scale and lcalize collider
            someoneWasHit = false;
            Vector3 colliderSize = new Vector3(attacker.attackSize.x, 2, attacker.attackSize.y);
            Vector3 colliderCenter = Vector3.zero;
            if (attacker.direction == Vector3.left || attacker.direction == Vector3.right || attacker.direction == Vector3.back || attacker.direction == Vector3.forward)
            {
                colliderCenter = attacker.worldPosition + (attacker.direction * (attacker.attackRange + colliderSize.z / 2));
            }
            else
            {
                colliderCenter = attacker.worldPosition + (attacker.direction * (attacker.attackRange + colliderSize.z / 2.85f));
            }
            Collider[] hittedColliders = Physics.OverlapBox(colliderCenter, colliderSize / 2, attacker.rotation);
            #endregion

            for (int i = 0; i < hittedColliders.Length; i++)
            {
                if (hittedColliders[i].tag == "Enemy" && attacker == PlayerLogic.player)
                {
                    for (int j = 0; j < EnemyLogic.enemies.Count; j++)
                    {
                        if (EnemyLogic.enemies[j] != null)
                        {
                            if (EnemyLogic.enemies[j].sceneObject == hittedColliders[i].gameObject)
                            {
                                colorableObjects.Add(EnemyLogic.enemies[j].sceneObject);
                                EnemyLogic.enemies[j].GetDamage(attacker.attackValue);

                                someoneWasHit = true;
                            }
                        }
                    }
                }
                else if (hittedColliders[i].tag == "Player" && attacker != PlayerLogic.player)
                {
                    PlayerLogic.player.GetDamage(attacker.attackValue);
                    someoneWasHit = true;
                }
            }
        }
        #endregion

        private static void GetDamage(this Lifeform victim, int baseDamage)
        {
            victim.actualHitpoints -= (baseDamage);
        }
    }
}


namespace CollisionDetection
{
    public static class Collision
    {
        public static bool IsColliding(this Lifeform lifeform, Vector3 forwardDirection)
        {
            bool isColliding = false;
            Vector3 rayPosition = lifeform.sceneObject.transform.position;
            Vector3 colliderSize = lifeform.sceneObject.transform.localScale / 2;
            Collider[] hitColliders = Physics.OverlapBox(rayPosition + forwardDirection / 4, colliderSize / 2, lifeform.sceneObject.transform.rotation, 256); // Layermask 256 8th index in the Engine Layers (1<<8)

            foreach (Collider collider in hitColliders)
            {
                isColliding = true;
                break;
            }
            return isColliding;
        }


        // Berücksichtigt, dass unser Objekt ausschließlich forward gehen kann
        public static Collider[] ForwardMoveWallCollision(GameObject moveObject, float speed)
        {
            Vector3 size = moveObject.transform.localScale;
            Vector3 A = moveObject.transform.position;
            Vector3 B = A + moveObject.transform.forward * Time.deltaTime * speed;                 // position next frame
            float dist = (B - A).magnitude;
            Vector3 halfZ = moveObject.transform.forward * moveObject.transform.localScale.z / 2;  //hitbox.extents.z;
            Vector3 origin = (A + (B - A) / 2) + halfZ;                                            
            Vector3 halfExtents = 0.5f * new Vector3(size.x, size.y, size.z * dist);

            Collider[] colliders = Physics.OverlapBox(origin, halfExtents, moveObject.transform.rotation, 256);// Layermask 256 -> Layer number 8 (1<<8)
            return colliders;
        }
    }
}