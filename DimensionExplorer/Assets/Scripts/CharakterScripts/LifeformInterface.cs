﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface Lifeform
{
    int maxHitpoints { get; set; }
    int actualHitpoints { get; set; }
    GameObject sceneObject { get; set; }
    int level { get; set; }
}