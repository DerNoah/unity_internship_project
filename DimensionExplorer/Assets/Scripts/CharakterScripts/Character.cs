﻿using System.Collections;
using UnityEngine;
using AttackLogic;
using CollisionDetection;

public class Character : Lifeform, CanDealDamage
{
    [SerializeField]
    public GameObject gameObject;
    public int level;
    public Weapon heldWeapon;
    public Vector3 lookDirection
    {
        get { return Vector3.Normalize(gameObject.transform.forward); }
    }

    private Sound weaponSound;

    //Der Pfad innerhalb des resources Ordners zum Spieler prefab
    private const string resourcesPathToGameObject = "";
    public KeyCode primaryAttackKey => KeyCode.J;
    public KeyCode secondaryAttackKey => KeyCode.K;
    public KeyCode tertiaryAttackKey => KeyCode.E;
    public KeyCode healKey => KeyCode.Q;
    public KeyCode evasionKey => KeyCode.LeftShift;
    public KeyCode actionKey => KeyCode.Space;

    #region hitPoints
    public int maxHitPoints;

    private int m_actualHitpoints;
    public int actualHitpoints
    {
        get { return m_actualHitpoints; }
        set
        {
            if (value > maxHitPoints)
            {
                m_actualHitpoints = maxHitPoints;
            }
            else
            {
                m_actualHitpoints = value;
                if (m_actualHitpoints <= 0)
                {
                    ActivateGameOver();
                }
            }
        }
    }
    #endregion

    #region lifeforminterface get and set
    int Lifeform.maxHitpoints
    {
        get { return this.maxHitPoints; }
        set { this.maxHitPoints = value; }
    }
    int Lifeform.actualHitpoints
    {
        get { return actualHitpoints; }
        set { this.actualHitpoints = value; }
    }
    GameObject Lifeform.sceneObject
    {
        get { return this.gameObject; }
        set { this.gameObject = value; }
    }
    int Lifeform.level
    {
        get { return this.level; }
        set { this.level = value; }
    }
    #endregion

    #region CanDealDamage get
    Vector3 CanDealDamage.worldPosition => gameObject.transform.position;
    Quaternion CanDealDamage.rotation => gameObject.transform.rotation;
    int CanDealDamage.attackValue => heldWeapon.attackValue;
    float CanDealDamage.attackRange => heldWeapon.attackRange;
    Vector3 CanDealDamage.attackSize => heldWeapon.attackSize;
    float CanDealDamage.attackspeed => heldWeapon.attackSpeed;
    Vector3 CanDealDamage.direction => lookDirection;
    #endregion


    #region movement
    Transform camTrans = Camera.main.transform;
    float lerpVal = 0.175f;
    Vector3 targetDirection = Vector3.zero;
    Vector3 moveDirection = Vector3.zero;
    public void DoMove(float speed)
    {
        bool leftKey = Input.GetKey(KeyCode.A) && !isInAttack && !isInEvasionRoll;
        bool rightKey = Input.GetKey(KeyCode.D) && !isInAttack && !isInEvasionRoll;
        bool forwardKey = Input.GetKey(KeyCode.W) && !isInAttack && !isInEvasionRoll;
        bool backKey = Input.GetKey(KeyCode.S) && !isInAttack && !isInEvasionRoll;

        Vector3 oldDirection = moveDirection;
        targetDirection = Vector3.zero;
        moveDirection = Vector3.zero;


        if (backKey)
        {
            targetDirection = -1 * new Vector3(camTrans.forward.x, 0, camTrans.forward.z);
        }
        if (forwardKey)
        {
            targetDirection = new Vector3(camTrans.forward.x, 0, camTrans.forward.z);
        }
        if (!PointInSameDirection(oldDirection, targetDirection))
        {
            oldDirection = targetDirection;
        }

        if (leftKey)
        {
            targetDirection -= camTrans.right;
        }
        if (rightKey)
        {
            targetDirection += camTrans.right;
        }

        if (targetDirection != Vector3.zero) // wenn es eine Eingabe gab
        {
            moveDirection = Vector3.Lerp(oldDirection, targetDirection.normalized, lerpVal);
            this.gameObject.transform.forward = moveDirection;                                                  // Dreht den Character in die MoveDirection. Drehen ist immer möglich, auch bei Kollision!
            Collider[] colliders = CollisionDetection.Collision.ForwardMoveWallCollision(this.gameObject.transform.gameObject, speed);
            if (colliders.Length == 0)
            {
                this.gameObject.transform.Translate(Vector3.forward * speed * Time.fixedDeltaTime, Space.Self);
            }
            else if (colliders.Length == 1)
            {
                this.gameObject.transform.Translate(ForwardMoveWallSlide(colliders, this.gameObject.transform.gameObject, speed), Space.World);
            }
        }
    }

    Vector3 ForwardMoveWallSlide(Collider[] colliders, GameObject gameObject, float speed)
    {
        Vector3 forward = gameObject.transform.forward;
        Vector3 normal = Vector3.zero;
        Vector3 slideMoveVector = Vector3.zero; // if you can't move, don't

        Collider collider = null;
        foreach (var item in colliders)
        {
            if (item != null)
            {
                collider = item;
                break;
            }
        }
        if (collider == null)
        {
            return Vector3.zero;
        }


        RaycastHit hitInfo;
        Vector3 from = gameObject.transform.position;
        Vector3 to = collider.ClosestPoint(gameObject.transform.position) - from;
        Ray ray = new Ray(from, to);
        float range = Vector3.Distance(from, to);
        if (collider.Raycast(ray, out hitInfo, range * 1.05f))
        {
            normal = hitInfo.normal;
        }
        else
        {
            Debug.Log("Why is the normal missing? How can that even be?");
            return Vector3.zero;
        }

        if (normal != Vector3.zero)
        {
            slideMoveVector = Vector3.ProjectOnPlane(forward, normal) * Time.deltaTime * speed;
        }
        else
        {
            Debug.Log("Lost the normal in the void. Tragic"); //Kann nur sein, wenn der Raycast getroffen hat, aber hitinfo keine Normale geliefert hat.
        }

        return slideMoveVector;
    }


    bool PointInSameDirection(Vector3 vecA, Vector3 vecB)
    {
        bool isSame = false;

        if (Vector3.Dot(vecA, vecB) > 0)
        {
            isSame = true;
        }
        return isSame;
    }
    #endregion

    #region evasionRoll logic
    public bool isInEvasionRoll;
    public IEnumerator EvasiveRoll()
    {
        Vector3 currentVelocity = new Vector3();
        Vector3 originPosition = gameObject.transform.position;
        Vector3 rayOriginPosition = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y - 0.5f, gameObject.transform.position.z);
        Vector3 targetPoint = this.gameObject.transform.position + (lookDirection * 4);

        //Shoots a Raycast in roll direction, if it collides it sets a new target point to the collisionpoint
        RaycastHit raycastHit;
        bool rayCast = Physics.Raycast(rayOriginPosition, lookDirection, out raycastHit, Vector3.Distance(gameObject.transform.position, targetPoint));
        if (rayCast)
        {
            targetPoint = originPosition + lookDirection * raycastHit.distance;
        }

        while (true)
        {
            if (!this.IsColliding(lookDirection))
            {
                this.gameObject.transform.position = Vector3.SmoothDamp(this.gameObject.transform.position, targetPoint, ref currentVelocity, Time.deltaTime * 5);
            }
            else
            {
                targetPoint = gameObject.transform.position;
            }

            if (Vector3.Distance(this.gameObject.transform.position, targetPoint) <= 0.05f) //Wenn die Rolle ausgeführt wurde, wird die Coroutine gestoppt
            {
                isInEvasionRoll = false;
                yield break;
            }
            else
            {
                isInEvasionRoll = true;
            }

            yield return new WaitForFixedUpdate();
        }
    }
    #endregion

    #region attack procedure
    public bool isInAttack;
    public IEnumerator AttackProcedure(CanDealDamage attacker)
    {
        bool someoneWasHit;
        isInAttack = true;
        yield return new WaitForSeconds(attacker.attackspeed);
        this.DoAttack(out someoneWasHit);
        //play a sound when an enemy was hit
        if (someoneWasHit)
        {
            if (this.heldWeapon is Sword)
            {
                weaponSound.PlaySoundEffect(Sword.swordHitted);
            }
        }
        else
        {
            if (this.heldWeapon is Sword)
            {
                weaponSound.PlaySoundEffect(Sword.swordDefault);
            }
        }

        //red hiteffect feedback it won´t work if the enemy is not an raw cube or something else.
        foreach (GameObject item in Attack.colorableObjects)
        {
            item.GetComponent<Renderer>().material.color = Color.red;
        }

        yield return new WaitForSeconds(attacker.attackspeed / 2); // wait for sound

        foreach (GameObject item in Attack.colorableObjects)
        {
            item.GetComponent<Renderer>().material.color = Color.white;
        }
        Attack.colorableObjects.Clear();

        isInAttack = false;
        yield break;
    }
    #endregion

    #region heal procedure
    public bool isHealing;
    public IEnumerator Heal()
    {
        if (this.actualHitpoints >= maxHitPoints)
        {
            yield break;
        }

        isHealing = true;
        float healStep = 0;
        yield return new WaitForSeconds(0.25f);

        if (Input.GetKey(healKey))
        {
            healStep = ((float)(maxHitPoints / 2) / 5);
        }
        else
        {
            isHealing = false;
            yield break;
        }

        for (int i = 0; i < 5; i++)
        {
            actualHitpoints += Mathf.RoundToInt(healStep);
            yield return new WaitForSeconds(0.2f);
        }

        isHealing = false;
    }
    #endregion


    void ActivateGameOver()
    {
        Debug.LogError("Du bist leider gerade gestorben :'(");
        Object.Destroy(this.gameObject, 0.5f);
        GameObject.Find("Logic").SetActive(false);
        Camera.main.GetComponent<FollowCam>().enabled = false;
    }


    public Character(int startLife)
    {
        this.maxHitPoints = startLife;
        this.m_actualHitpoints = maxHitPoints;
        this.gameObject = GameObject.CreatePrimitive(PrimitiveType.Capsule);
        this.gameObject.transform.position = new Vector3(-133, 1, -12.5f);
        this.gameObject.tag = "Player";
        this.gameObject.name = "Player";

        weaponSound = new Sound();
        weaponSound.soundEffect = this.gameObject.AddComponent<AudioSource>();
    }
}
