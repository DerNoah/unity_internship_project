﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gravity;

public class PlayerLogic : MonoBehaviour
{
    public static Character player;
    public float playerSpeed = 10;


    void Start()
    {
        player = new Character(100);
        Invoke("GivePlayerSword", 0.1f);
        StartCoroutine(player.UseGravity());
        Camera.main.gameObject.AddComponent<FollowCam>();
    }

    void GivePlayerSword() { player.heldWeapon = WeaponInstances.allSwords[1]; }


    void Update()
    {
        //Debug.Log(player.lookDirection);
        if (Input.GetKeyDown(player.primaryAttackKey) && !player.isInAttack)
        {
            StartCoroutine(player.AttackProcedure(player));
        }

        player.DoMove(playerSpeed);

        if (Input.GetKeyDown(player.evasionKey) && !player.isInEvasionRoll)
        {
            StartCoroutine(player.EvasiveRoll());
        }

        if (Input.GetKeyDown(player.healKey) && !player.isHealing)
        {
            StartCoroutine(player.Heal());
        }
    }


    //private void OnDrawGizmos()
    //{
    //    Gizmos.color = Color.blue;
    //    Gizmos.DrawCube(player.gameObject.transform.position, player.gameObject.transform.localScale);
    //}
}
