﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface CanDealDamage
{
    Vector3 worldPosition { get; }
    Quaternion rotation { get; }
    int attackValue { get; }
    /// <summary>
    /// distance from the attack
    /// </summary>
    float attackRange { get;}
    /// <summary>
    /// the area where the attack takes place
    /// </summary>
    Vector3 attackSize { get; }
    /// <summary>
    /// the lower the value is, the faster the attack
    /// </summary>
    float attackspeed { get; }
    Vector3 direction { get; }
}

//Noah Plützer