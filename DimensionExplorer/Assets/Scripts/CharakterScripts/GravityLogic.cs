﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gravity
{
    public static class GravityLogic
    {
        public static IEnumerator UseGravity(this Lifeform lifeform)
        {
            float fallingTime = 0;
            while (true)
            {
                Vector3 scale = lifeform.sceneObject.transform.localScale;
                bool rayCast = Physics.Raycast(lifeform.sceneObject.transform.position, Vector3.down, out RaycastHit raycastHit);

                Collider[] grounds = Physics.OverlapBox(lifeform.sceneObject.transform.position, scale / 2, lifeform.sceneObject.transform.rotation, 512); // layervalue 512 is the 9th index of the engine layer list

                bool isCollided = false;


                if (grounds.Length > 0)
                {
                    isCollided = true;
                }

                //foreach (Collider collider in grounds)
                //{
                //    //if (collider.transform.tag == "Ground" || collider.transform.tag == "Wall")
                //    //{
                //        isCollided = true;
                //        break;
                //    }
                //}

                if (raycastHit.distance < scale.y + 0.001f && rayCast)
                {
                    lifeform.sceneObject.transform.position = new Vector3(lifeform.sceneObject.transform.position.x,
                                                                lifeform.sceneObject.transform.position.y + (scale.y - raycastHit.distance),
                                                                lifeform.sceneObject.transform.position.z);

                    fallingTime = 0;
                }
                else if (!isCollided)
                {
                    lifeform.sceneObject.transform.Translate(Vector3.down * 10 * fallingTime * Time.deltaTime);
                }

                fallingTime += Time.deltaTime;
                yield return new WaitForFixedUpdate();
            }
        }
    }
}
