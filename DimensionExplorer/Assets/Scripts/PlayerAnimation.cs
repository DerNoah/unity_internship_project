﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimation : MonoBehaviour
{
    public GameObject rightShoulder;
    public GameObject lowerArmRightParent;
    public GameObject upperArmRight;

    public GameObject leftShoulder;
    public GameObject lowerArmLeftParent;
    public GameObject upperArmLeft;
    

    void Start()
    {
        
    }



    void Update()
    {
        lowerArmRightParent.transform.position = upperArmRight.transform.TransformPoint(new Vector3(0, 0, 0.65f));
        lowerArmRightParent.transform.rotation = rightShoulder.transform.rotation;

        lowerArmLeftParent.transform.position = upperArmLeft.transform.TransformPoint(new Vector3(0, 0, 0.65f));
        lowerArmLeftParent.transform.rotation = leftShoulder.transform.rotation;
    }
}
