﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeonRoom
{
    Wall[] walls;
    List<Tile> tiles = new List<Tile>();

    public void SetWallsAtMap()
    {
        this.walls = RandomWallGenerator();
        bool isVertical = true;

        for (int i = 0; i < walls.Length; i++)
        {
            float wallScaleX = Random.Range(4, 10);
            float wallScaleY = 5f;
            float wallScaleZ = Random.Range(4, 10);


            if (i == 0)
            {
                walls[i] = new Wall(GameObject.CreatePrimitive(PrimitiveType.Cube), new Vector3(wallScaleX, wallScaleY, 1), new Vector3(i, wallScaleY / 2, i));
            }

            else if (isVertical)
            {
                walls[i] = new Wall(GameObject.CreatePrimitive(PrimitiveType.Cube), new Vector3(1, wallScaleY, wallScaleZ), new Vector3(walls[i - 1].wall.transform.position.x + (walls[i - 1].wall.transform.localScale.x / 2) + 0.5f, wallScaleY / 2, walls[i - 1].wall.transform.position.z - wallScaleZ / 2 + 0.5f));
                isVertical = !isVertical;
            }

            else
            {
                walls[i] = new Wall(GameObject.CreatePrimitive(PrimitiveType.Cube), new Vector3(wallScaleX, wallScaleY, 1), new Vector3(walls[i - 1].wall.transform.position.x - wallScaleX / 2 + 0.5f, wallScaleY / 2, walls[i - 1].wall.transform.position.z + (walls[i - 1].wall.transform.localScale.z / 2) + 0.5f));
                isVertical = !isVertical;
            }
        }
    }


    Wall[] RandomWallGenerator()
    {
        int evenNumber;
        evenNumber = 4/*Random.Range(4, 13)*/;

        if (evenNumber % 2 == 1)
        {
            evenNumber -= 1;
            walls = new Wall[evenNumber];
            return walls;
        }

        else
        {
            walls = new Wall[evenNumber];
            return walls;
        }
    }
}

public class Wall
{
    public GameObject wall;

    public Wall(GameObject wall, Vector3 wallScale, Vector3 wallPos)
    {
        this.wall = wall;
        this.wall.transform.localScale = wallScale;
        this.wall.transform.position = wallPos;
    }
}

public class Tile
{
    public GameObject tile;
    public Vector3 tilePos;

    public Tile(GameObject tile, Vector3 tilePos)
    {
        this.tile = tile;
        this.tilePos = tilePos;
    }
}
