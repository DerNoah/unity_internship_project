﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCam : MonoBehaviour
{
    public float smoothness = 0.1f;
    private float dist = 7f; // Abstand vom Dolly zum Character, positiv, dist und height sind die Werte zu Beginn der Szene.
    private float height = 3f; // Höhe der Kamera zum Dolly

    private Transform cam;
    private Transform character;
    private Vector3 dolly; // Punkt auf dem Boden unter der Kamera 
    private Vector3 target; // Punkt, der sich dem Character langsam annähert. 

    void Awake()
    {
        cam = this.transform;
        character = PlayerLogic.player.gameObject.transform;
        ResetValues();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            ResetValues();
        }

        cam.LookAt(character);
        target = Vector3.Lerp(target, character.position, smoothness);
        //dolly = Vector3.Lerp(dolly, target, 1 - dist/Vector3.Distance(dolly, target)); // Position auf der Geraden - dolly <-> Target bei Verhältnis von Soll-Abstand zu Ist-Abstand
        dolly = target - (target - dolly).normalized * dist;
        cam.position = dolly + (Vector3.up * height);
    }



    void ResetValues()
    {
        cam.position = character.position + character.forward * -1 * dist + new Vector3(0, height, 0);
        target = character.position;
        dolly = cam.position + Vector3.down * height;
        dist = Vector3.Distance(target, dolly); // ebenerdiger Abstand zum Ziel
    }
}

//Written by Sebastian Neuhaus