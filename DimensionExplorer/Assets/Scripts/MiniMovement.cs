﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniMovement : MonoBehaviour
{
    public GameObject parent;
    public float speed;
    bool isMoving;

    void Start()
    {

    }

    void Update()
    {
        if (isMoving)
        {
            parent.transform.Translate(Vector3.forward * speed * Time.deltaTime, Space.Self);
        }
    }

    public void StartMoving()
    {
        isMoving = true;
    }

    public void EndMoving()
    {
        isMoving = false;
    }
}
