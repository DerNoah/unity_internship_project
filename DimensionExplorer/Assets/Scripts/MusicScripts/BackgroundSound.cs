﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundSound : MonoBehaviour
{
    AudioClip[] backgroundMusic;
    AudioSource audioSource;
    public GameObject inGamePanel;
    public GameObject loadingPanel;

    void Start()
    {
        loadingPanel.SetActive(false);
        inGamePanel.SetActive(true);
        backgroundMusic = Resources.LoadAll<AudioClip>("Music/DungeonBackground");
        audioSource = GameObject.Find("Logic").GetComponent<AudioSource>();

        StartCoroutine(playSound());
        GameIsStarting();
    }


    IEnumerator playSound()
    {
        while (true)
        {
            audioSource.clip = backgroundMusic[Random.Range(0, backgroundMusic.Length)];
            audioSource.volume = 0.02f;
            audioSource.Play();
            yield return new WaitUntil(() => !audioSource.isPlaying);
        }
    }

    void GameIsStarting()
    {
        loadingPanel.SetActive(false);
        inGamePanel.SetActive(true);
    }
}
