﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class Sound
{
    public AudioSource soundEffect;

    public void PlaySoundEffect(AudioClip effect)
    {
        soundEffect.clip = effect;        
        soundEffect.loop = false;
        soundEffect.volume = 0.1f;
        soundEffect.Play();
    }
}
