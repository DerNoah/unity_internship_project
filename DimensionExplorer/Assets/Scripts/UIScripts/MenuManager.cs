﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Animations;
using UnityEditor;

public class MenuManager : MonoBehaviour
{
    public GameObject inGamePanel;
    public GameObject menuPanel;
    public GameObject waitingScreen;
    public GameObject logic;
    public GameObject optionPanel;
    public GameObject creditsPanel;
    public GameObject audioPanel;
    public GameObject keyBindingsPanel;
    public GameObject pausePanel;

    bool fromPause;

    void Update()
    {
        Camera.main.transform.RotateAround(new Vector3(-30, 0, 0), Vector3.up, 30 * Time.deltaTime);
    }

    public void StartGame()
    {
        for (int i = 0; i < logic.GetComponents<MonoBehaviour>().Length; i++)
        {
            logic.GetComponents<MonoBehaviour>()[i].enabled = true;
        }
        waitingScreen.SetActive(true);
        menuPanel.SetActive(false);
    }

    public void SwitchOptions()
    {
        menuPanel.SetActive(false);
        optionPanel.SetActive(true);
    }

    public void SwitchBackFromOptionToMenu()
    {
        optionPanel.SetActive(false);
        if (fromPause)
        {
            pausePanel.SetActive(true);
            fromPause = false;
        }

        else
        {
            menuPanel.SetActive(true);
        }
    }

    public void SwitchBackFromCreditsToMenu()
    {
        creditsPanel.SetActive(false);
        menuPanel.SetActive(true);
    }

    public void SwitchBackFromAudioToOptions()
    {
        audioPanel.SetActive(false);
        optionPanel.SetActive(true);
    }

    public void SwitchBackFromKeyBindingsToOptions()
    {
        keyBindingsPanel.SetActive(false);
        optionPanel.SetActive(true);
    }

    public void SwitchBackFromPauseToMenu()
    {
        logic.GetComponent<PauseUI>().enabled = false;
        pausePanel.SetActive(false);
        menuPanel.SetActive(true);
    }

    public void SwitchPauseToOption()
    {
        pausePanel.SetActive(false);
        optionPanel.SetActive(true);
        fromPause = true;
    }

    public void SwitchAudio()
    {
        optionPanel.SetActive(false);
        audioPanel.SetActive(true);
    }

    public void SwitchCredits()
    {
        menuPanel.SetActive(false);
        creditsPanel.SetActive(true);
    }

    public void SwitchKeyBindings()
    {
        optionPanel.SetActive(false);
        keyBindingsPanel.SetActive(true);
    }

    public void ExitGame()
    {
        //Application.Quit();
        EditorApplication.isPlaying = false;
    }
}
