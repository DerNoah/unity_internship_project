using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class EnemyManager
{
    public static void EnemySpawn(GameObject ground, int enemyCount, List<Enemy> enemies)
    {
        float groundHigh = ground.transform.position.y;
        Enemy enemy;

        for (int i = 0; i < enemyCount; i++)
        {
            float randomSpawnPointX = ground.transform.position.x + Random.Range(-3, 4);
            float randomSpawnPointZ = ground.transform.position.z + Random.Range(-1, 6);
            
            enemy = new Enemy(GameObject.CreatePrimitive(PrimitiveType.Cube), new Vector3(randomSpawnPointX, groundHigh, randomSpawnPointZ), 2);
            enemies.Add(enemy);
            enemy.sceneObject.transform.position = new Vector3(enemy.sceneObject.transform.position.x, enemy.sceneObject.transform.position.y + enemy.sceneObject.transform.localScale.y, enemy.sceneObject.transform.position.z);
        }
    }
}