﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AttackLogic;

public class Enemy : Lifeform, CanDealDamage
{
    #region LifeformProperties
    public GameObject sceneObject;
    public int hitpoints;
    public int actualHitpoints;
    public int level;
    Coroutine attackCoroutine;

    GameObject Lifeform.sceneObject
    {
        get { return this.sceneObject; }
        set { this.sceneObject = value; }
    }
    int Lifeform.maxHitpoints
    {
        get { return this.hitpoints; }
        set { this.hitpoints = value; }
    }

    int Lifeform.actualHitpoints
    {
        get { return this.actualHitpoints; }
        set
        {
            this.actualHitpoints = value;
            if (actualHitpoints <= 0)
            {
                if (attackCoroutine != null)
                {
                    EnemyLogic.mono.StopCoroutine(attackCoroutine);
                }
                for (int i = 0; i < EnemyLogic.enemies.Count; i++)
                {
                    if (EnemyLogic.enemies[i] == this)
                    {
                        Attack.colorableObjects.Remove(this.sceneObject);
                        EnemyLogic.enemies[i] = null;
                        EnemyLogic.enemies.Remove(this);
                        break;
                    }
                }
                EnemyLogic.mono.StartCoroutine(DieProcedure());
            }
        }
    }

    int Lifeform.level
    {
        get { return this.level; }
        set { this.level = value; }
    }
    #endregion

    # region CanDealDamageProperties
    public Vector3 worldPosition;
    public Quaternion rotation;
    public int attackValue;
    public float attackRange;
    public Vector3 attackSize;
    public float attackspeed;
    public Vector3 direction;


    Vector3 CanDealDamage.worldPosition => sceneObject.transform.position;
    Quaternion CanDealDamage.rotation => sceneObject.transform.rotation;
    Vector3 CanDealDamage.direction => sceneObject.transform.forward;

    int CanDealDamage.attackValue
    {
        get { return this.attackValue; }
    }
    float CanDealDamage.attackRange
    {
        get { return this.attackRange; }
    }
    Vector3 CanDealDamage.attackSize
    {
        get { return this.attackSize; }
    }
    float CanDealDamage.attackspeed
    {
        get { return this.attackspeed; }
    }
    #endregion

    public Enemy(GameObject enemy, Vector3 enemyPos, int level)
    {
        this.sceneObject = enemy;
        this.sceneObject.transform.position = enemyPos;
        this.sceneObject.transform.localScale = new Vector3(0.7f, 0.7f, 0.7f);
        this.level = level;
        this.sceneObject.tag = "Enemy";
        this.hitpoints = 10;
        this.actualHitpoints = this.hitpoints;
        this.attackValue = 2;
        this.attackspeed = 2;
        this.attackRange = 0.5f;
        this.attackSize = Vector3.one;
        //this.sceneObject.AddComponent<Rigidbody>();
        //this.sceneObject.GetComponent<Rigidbody>().useGravity = false;
    }

    public void DoMove(float speed)
    {
        if (Vector3.Distance(this.sceneObject.transform.position, PlayerLogic.player.gameObject.transform.position) <= 8f)
        {
            this.sceneObject.transform.LookAt(new Vector3(PlayerLogic.player.gameObject.transform.position.x, this.sceneObject.transform.position.y, PlayerLogic.player.gameObject.transform.position.z));

            RaycastHit hit;
            if (!(Physics.Raycast(this.sceneObject.transform.position, this.sceneObject.transform.forward, out hit, (this.sceneObject.transform.localScale.x / 2) + this.attackRange + (PlayerLogic.player.gameObject.transform.localScale.x / 2))))
            {
                this.sceneObject.transform.position = Vector3.Lerp(this.sceneObject.transform.position, PlayerLogic.player.gameObject.transform.position, speed * Time.deltaTime);
            }

            else if ((Physics.Raycast(this.sceneObject.transform.position, this.sceneObject.transform.forward, out hit, (this.sceneObject.transform.localScale.x / 2) + this.attackRange + (PlayerLogic.player.gameObject.transform.localScale.x / 2))))
            {
                EnemyAttackPlayer(hit);
            }
        }
    }

    void DropWeapon(Sword sword)
    {
        //sword = new Sword();
    }

    void EnemyLeveln(int DungeonNumber)
    {
        this.level += Random.Range(1, 3);
        this.hitpoints += Random.Range(10, 21);
        this.attackValue += Random.Range(1, 3);
    }

    void EnemyAttackPlayer(RaycastHit hit)
    {
        if (hit.collider.gameObject == PlayerLogic.player.gameObject && attackCoroutine == null)
        {
            attackCoroutine = EnemyLogic.mono.StartCoroutine(AttackProcedure(this));
        }
        else if (attackCoroutine != null && hit.collider.gameObject != PlayerLogic.player.gameObject)
        {
            EnemyLogic.mono.StopCoroutine(this.attackCoroutine);
        }

        else if ((hit.collider.gameObject != PlayerLogic.player.gameObject && attackCoroutine != null))
        {
            EnemyLogic.mono.StopCoroutine(attackCoroutine);
            attackCoroutine = null;
        }
    }

    public IEnumerator AttackProcedure(CanDealDamage enemy)
    {
        while (true)
        {
            yield return new WaitForSeconds(enemy.attackspeed / 2);
            this.DoAttack();
        }
    }


    public IEnumerator DieProcedure()
    {
        GameObject soundRemnand = new GameObject();
        soundRemnand.transform.position = this.sceneObject.transform.position;

        Object.Destroy(this.sceneObject);
        EnemyLogic.DropWeapon(this.sceneObject);

        Sound newSound = new Sound();
        newSound.soundEffect = soundRemnand.AddComponent<AudioSource>();
        newSound.PlaySoundEffect((AudioClip)Resources.Load("Sounds/swordKilled"));

        yield return new WaitUntil(() => !newSound.soundEffect.isPlaying);

        Object.Destroy(soundRemnand);

        yield break;
    }
}
