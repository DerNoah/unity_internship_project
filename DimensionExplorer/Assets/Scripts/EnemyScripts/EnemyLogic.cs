﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CollisionDetection;

public class EnemyLogic : MonoBehaviour
{
    Enemy enemy;
    private GameObject[] grounds;
    public static List<Enemy> enemies = new List<Enemy>();
    public static MonoBehaviour mono;

    void Start()
    {
        grounds = GameObject.FindGameObjectsWithTag("Ground");
        mono = this;

        for (int i = 0; i < grounds.Length; i++)
        {
            EnemyManager.EnemySpawn(grounds[i], 20, enemies);
        }
    }
    
    void Update()
    {
        if (enemies.Count != 0)
        {
            foreach (Enemy enemy in enemies)
            {
                if (enemy != null)
                {
                    enemy.DoMove(1);
                }
            }
        }
    }

    public static void DropWeapon(GameObject enemy)
    {
        int dropRate;

        dropRate = Random.Range(0, 100);

        if (dropRate <= 10)
        {
            GameObject weapon = GameObject.CreatePrimitive(PrimitiveType.Capsule);
            weapon.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
            weapon.transform.position = enemy.transform.position;
        }
    }
}
