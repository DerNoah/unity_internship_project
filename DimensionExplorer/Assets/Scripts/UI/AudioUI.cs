﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class AudioUI : MonoBehaviour
{
    public GameObject logic;
    AudioSource audioSource;
    public Slider volumeSlider;
    public GameObject volumeText;

    void Start()
    {
        GetVolume();
    }


    void Update()
    {
        SetVolume();
    }

    void GetVolume()
    {
        audioSource = logic.GetComponent<AudioSource>();
        volumeSlider.value = audioSource.volume;
        volumeText.GetComponent<TextMeshProUGUI>().text = ((int)(volumeSlider.value * 100)).ToString() + " %";
    }

    void SetVolume()
    {
        audioSource.volume = volumeSlider.value;
        volumeText.GetComponent<TextMeshProUGUI>().text = ((int)(volumeSlider.value * 100)).ToString() + " %";
    }
}
