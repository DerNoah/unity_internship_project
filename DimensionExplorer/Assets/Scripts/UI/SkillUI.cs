﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillUI : MonoBehaviour
{
    List<Sprite> skillsprites;
    public Image fireImage;
    public Image iceImage;
    public Image boostImage;
    public Image shieldImage;


    void Start()
    {
        skillsprites = new List<Sprite>(Resources.LoadAll<Sprite>("Skills/"));
        SetSpriteInImage("Fire", fireImage, skillsprites);
        SetSpriteInImage("Ice", iceImage, skillsprites);
        SetSpriteInImage("Boost", boostImage, skillsprites);
        SetSpriteInImage("Shield", shieldImage, skillsprites);
    }


    void Update()
    {
        
    }

    void SetSpriteInImage(string spriteName, Image image, List<Sprite> sprites)
    {
        for (int i = 0; i < sprites.Count; i++)
        {
            if (spriteName == sprites[i].name)
            {
                image.sprite = sprites[i];
            }
        }
    }
}
