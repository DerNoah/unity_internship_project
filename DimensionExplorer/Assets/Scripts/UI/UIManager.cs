﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    GameObject player;
    List<Enemy> enemies;
    public Slider lifeSlider;
    public Text lifeScreen;
    List<Image> images;

    public Image qImage;
    public Image eImage;
    public Image jImage;
    public Image kImage;

    void Awake()
    {
        player = PlayerLogic.player.gameObject;
        enemies = EnemyLogic.enemies;
        SetImageOnScreen(qImage, eImage, jImage, kImage);
    }


    void Update()
    {
        SliderManager(lifeSlider);
        SetLifeScreenValues(lifeScreen, lifeSlider);
    }

    void SetEnemyAText()
    {
        for (int i = 0; i < enemies.Count; i++)
        {

        }
    }

    void SliderManager(Slider lifeSlider)
    {
        lifeSlider.maxValue = PlayerLogic.player.maxHitPoints;
        lifeSlider.minValue = 0;
        lifeSlider.value = PlayerLogic.player.actualHitpoints;
    }

    void SetLifeScreenValues(Text text, Slider slider)
    {
        text.text = slider.value.ToString() + " / " + slider.maxValue.ToString();
    }

    void SetImageOnScreen(Image q, Image e, Image j, Image k)
    {
        q.sprite = Resources.Load<Sprite>("Items/HealPoison");
        e.sprite = Resources.Load<Sprite>("Weapon/Shurikan");
        j.sprite = Resources.Load<Sprite>("Weapon/Sword");
    }
}
