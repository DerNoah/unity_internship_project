﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Animations;
using TMPro;

public class PauseUI : MonoBehaviour
{
    bool pause;
    public GameObject inGamePanel;
    public GameObject pausePanel;
    TextMeshProUGUI playerStats;
    public GameObject text;
    public static GameObject pausePanelOne;

    void Start()
    {
        pausePanelOne = pausePanel;
        playerStats = text.GetComponent<TextMeshProUGUI>();
    }

    void Update()
    {
        StartPause();
        SetStats();
    }

    void StartPause()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            pause = true;
        }

        switch (pause)
        {
            case false:
                inGamePanel.SetActive(true);
                pausePanel.SetActive(false);
                break;

            case true:
                if (Input.GetKeyDown(KeyCode.Escape) && pausePanel.activeInHierarchy)
                {
                    pause = false;
                }
                inGamePanel.SetActive(false);
                pausePanel.SetActive(true);
                break;
        }
    }

    public void ContinueGame()
    {
        pause = false;
    }

    void SetStats()
    {
        if (playerStats != null)
        {
            playerStats.text = "Lifepoints: " + PlayerLogic.player.actualHitpoints;
        }
    }
}
