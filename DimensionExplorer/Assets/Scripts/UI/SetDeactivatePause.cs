﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetDeactivatePause : MonoBehaviour
{
    public void DeactivatePanel()
    {
        PauseUI.pausePanelOne.SetActive(false);
    }
}
