﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class KeyBindingsUI : MonoBehaviour
{
    public GameObject forwardText;
    public GameObject backText;
    public GameObject rightText;
    public GameObject leftText;
    public GameObject fightText;
    public GameObject healText;
    public GameObject skillText;
    public GameObject itemsText;
    public GameObject evasionRollText;

    void Start()
    {
        SetKey(forwardText, "W");
        SetKey(backText, "S");
        SetKey(rightText, "D");
        SetKey(leftText, "A");
        SetKey(fightText, "J");
        SetKey(healText, "Q");
        SetKey(skillText, "K");
        SetKey(itemsText, "E");
        SetKey(evasionRollText, "Shift");
    }
    
    void Update()
    {
        
    }

    void SetKey(GameObject text, string key)
    {
        text.GetComponent<TextMeshProUGUI>().text = key;
    }
}
