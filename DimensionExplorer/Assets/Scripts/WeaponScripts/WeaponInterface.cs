﻿using UnityEngine;


public enum Rarity
{
    usually,
    rare,
    epic,
    legendary
}


public interface Weapon
{
    string name { get; }
    int attackValue { get; }
    /// <summary>
    /// distance from the attack
    /// </summary>
    float attackRange { get; }
    /// <summary>
    /// the area where the attack takes place
    /// </summary>
    Vector3 attackSize { get; }
    float attackSpeed { get; }     //Angriffs abklinkzeit, wie lange er nach eines Angriffs braucht um wieder Angreifen zu können
    Vector3 direction { get; }
    Rarity weaponRarity { get; set; }
}

[System.Serializable]
public class Sword : Weapon
{
    public string name;
    [SerializeField]
    int attackValue;
    [SerializeField]
    float attackRange;
    /// <summary>
    /// the area where the attack takes place
    /// </summary>
    [SerializeField]
    Vector2 attackSize;
    [SerializeField]
    float attackSpeed;
    [SerializeField]
    Vector3 direction;
    [SerializeField]
    Rarity rarity;

    public static readonly AudioClip swordDefault = (AudioClip)Resources.Load("Sounds/swordDefault");
    public static readonly AudioClip swordHitted = (AudioClip)Resources.Load("Sounds/swordHitted");
    public static readonly AudioClip swordKilled = (AudioClip)Resources.Load("Sounds/swordKilled");

    #region get for weaponinterface
    string Weapon.name => this.name;
    int Weapon.attackValue => this.attackValue;
    float Weapon.attackRange => this.attackRange;
    Vector3 Weapon.attackSize => this.attackSize;
    float Weapon.attackSpeed => this.attackSpeed;
    Vector3 Weapon.direction => this.direction;
    Rarity Weapon.weaponRarity
    {
        get { return this.rarity; }
        set { this.rarity = value; }
    }
    #endregion

    public Sword()
    {
        this.name = "MissingSword";
        this.attackValue = 1;
        this.attackRange = 1;
        this.attackSize = new Vector2(1, 1);
        this.attackSpeed = 1;
        this.direction = Vector3.forward;
        this.rarity = Rarity.usually;
    }
}


//string name, int attackValue, float attackRange, Vector2 attackSize, float attackSpeed, Vector3 direction, Rarity rarity
//"SimpleSword", 100, 2, new Vector2(2, 1), 0.2f, Vector3.forward