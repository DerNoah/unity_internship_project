﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEditor;

public class WeaponInstances : MonoBehaviour
{
    public static List<Sword> allSwords = new List<Sword>();

    void Start()
    {
        LoadWeapons();
    }


    private void LoadWeapons()
    {
        foreach (TextAsset item in Resources.LoadAll<TextAsset>("JsonFiles/"))
        {
            string textAsset = File.ReadAllText(Application.dataPath + "/Resources/JsonFiles/" + item.name + ".json");
            
            if (item.name.Contains("Sword"))
            {
                Sword newSword = new Sword();

                newSword = JsonUtility.FromJson<Sword>(textAsset);
                allSwords.Add(newSword);
            }
            else if (item.name.Contains(""))
            {
                continue;
                //Noch nicht implementierte Waffen wie z.B. ein Bogen
            }
        }
    }
}
